package main.menu;

import java.util.Scanner;

public class MainMenu implements Menu {

    private String menuHeader = "Choose option: ";
    private String menuOption1 = "1. Rent a bike";
    private String menuOption2 = "2. Add funds to your wallet";
    private String menuOption3 = "3. Stop renting";
    private String menuOption4 = "4. Exit application";
    public static int option;

    @Override
    public void runMenu() {
        showMenuOptions();
        returnChosenOption();
    }

    @Override
    public void showMenuOptions() {
        System.out.println(menuHeader);
        System.out.println(menuOption1);
        System.out.println(menuOption2);
        System.out.println(menuOption3);
        System.out.println(menuOption4);
    }

    @Override
    public int returnChosenOption() {
        Scanner scanner = new Scanner(System.in);
        do {
            option = scanner.nextInt();
            continue;
        } while (option != 1 && option != 2 && option != 3 && option != 4);
        return option;
    }
}
