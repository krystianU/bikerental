package main.menu;

public interface Menu {

    void runMenu();
    void showMenuOptions();
    int returnChosenOption();
}
