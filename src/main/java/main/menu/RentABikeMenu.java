package main.menu;

import java.util.Scanner;

public class RentABikeMenu implements Menu {

    private String menuHeader = "Choose option: ";
    private String menuOption1 = "1. Filter bikes";
    private String menuOption2 = "2. Choose a bike to rent";
    private String menuOption3 = "3. Previous menu";
    public static int option;


    @Override
    public void runMenu() {
        showMenuOptions();
        returnChosenOption();
    }

    @Override
    public void showMenuOptions() {
        System.out.println(menuHeader);
        System.out.println(menuOption1);
        System.out.println(menuOption2);
        System.out.println(menuOption3);

    }

    @Override
    public int returnChosenOption() {
        Scanner scanner = new Scanner(System.in);
        do {
            option = scanner.nextInt();
            continue;
        } while (option != 1 && option != 2 && option != 3);
        return option;
    }
}

