package main.bikeDatabase;

public class Bike {
    private int id;
    private String bikeBrand;
    private String bikeColor;
    private boolean isOccupied;
    private double pricePerHour;

    public Bike(int id, String bikeBrand, String bikeColor, double pricePerHour) {
        this.id = id;
        this.bikeBrand = bikeBrand;
        this.bikeColor = bikeColor;
        this.isOccupied = false;
        this.pricePerHour = pricePerHour;
    }

    public String getBikeBrand() {
        return bikeBrand;
    }

    public void setBikeBrand(String bikeBrand) {
        this.bikeBrand = bikeBrand;
    }

    public String getBikeColor() {
        return bikeColor;
    }

    public void setBikeColor(String bikeColor) {
        this.bikeColor = bikeColor;
    }

    public boolean isOccupied() {
        return isOccupied;
    }

    public void setOccupied(boolean occupied) {
        isOccupied = occupied;
    }

    public double getPricePerHour() {
        return pricePerHour;
    }

    public void setPricePerHour(double pricePerHour) {
        this.pricePerHour = pricePerHour;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Bike{" +
                "id=" + id +
                ", bikeBrand='" + bikeBrand + '\'' +
                ", bikeColor='" + bikeColor + '\'' +
                ", isOccupied=" + isOccupied +
                ", pricePerHour=" + pricePerHour +
                '}' + "\n";

    }

}
