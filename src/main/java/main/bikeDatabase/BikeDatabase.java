package main.bikeDatabase;

import java.util.LinkedList;
import java.util.List;

public class BikeDatabase {

    public List<Bike> createBikes() {
        List<Bike> bikesList = new LinkedList<>();
        bikesList.add(new Bike(1, "Bike_1", "Red", 2.0d));
        bikesList.add(new Bike(2, "Bike_2", "Green", 3.1d));
        bikesList.add(new Bike(3, "Bike_3", "White", 2.9d));
        bikesList.add(new Bike(4, "Bike_4", "Blue", 4.2d));
        bikesList.add(new Bike(5, "Bike_5", "Yellow", 5.0d));
        bikesList.add(new Bike(6, "Bike_6", "Black", 3.3d));
        bikesList.add(new Bike(7, "Bike_7", "Red", 2.4d));
        bikesList.add(new Bike(8, "Bike_8", "Blue", 1.5d));
        bikesList.add(new Bike(9, "Bike_9", "Green", 3.5d));
        bikesList.add(new Bike(10, "Bike_10", "Red", 6.0d));

        return bikesList;
    }


}
