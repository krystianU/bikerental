package main;


import main.bikeDatabase.Bike;
import main.bikeDatabase.BikeDatabase;
import main.menu.MainMenu;
import main.menu.RentABikeMenu;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class RentController {

    private Person person;
    private BikeDatabase bikeDatabase = new BikeDatabase();
    private List<Bike> bikeList = bikeDatabase.createBikes();
    private LocalTime startRenting, stopRenting;
    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss", Locale.US);
    private int rentedBikeId;
    private double totalCost;
    private MainMenu mainMenu = new MainMenu();
    private RentABikeMenu rentABikeMenu = new RentABikeMenu();

    public void runProgram() {
        newClient();
        runMenu();
    }

    private void runMenu() {
        mainMenu.runMenu();
        reactOnChosenOptionFromMainMenu();
    }

    private void bikeReservationMenu() {
        rentABikeMenu.showMenuOptions();
        reactOnChosenOptionFromRentABikeMenu();

    }

    private void rentABike() {
        showListOfBikes();
        reservedBike();
        startRenting();
        runMenu();
        System.out.println();
    }

    private void filterBikes() {
//to be implemented
    }

    private void summarizeRenting() {
        stopRenting = LocalTime.now();
        if (calculateRentCosts() != true) {
            runMenu();
        } else {
            showRentSummary();
            runMenu();
        }
    }

    private void reactOnChosenOptionFromMainMenu() {
        switch (MainMenu.option) {
            case 1:
                bikeReservationMenu();
                break;
            case 2:
                addMoneyToTheWallet();
                break;
            case 3:
                summarizeRenting();
                break;
            case 4:
                exitApplication();
                break;
            default:
                System.out.println("Not recognized option, try again");
                reactOnChosenOptionFromMainMenu();
        }
    }

    private void reactOnChosenOptionFromRentABikeMenu() {
        switch (rentABikeMenu.returnChosenOption()) {
            case 1:
                rentABike(); // <- filtering, not yet implemented
                break;
            case 2:
                rentABike();
                break;
            case 3:
                runMenu();
                break;
            default:
                System.out.println("Not recognized option, try again");
                reactOnChosenOptionFromRentABikeMenu();
        }
    }

    private void exitApplication() {
        return;
    }


    private void newClient() {
        Scanner s = new Scanner(System.in);
        System.out.println("Hello, please provide your wallet size: ");
        double amountOfMoney = s.nextDouble();
        person = new Person(amountOfMoney);
    }


    private void showListOfBikes() {

        System.out.println(bikeList);
    }

    private void reservedBike() {
        Scanner s = new Scanner(System.in);
        System.out.println("Which bike you'd like to choose? Type id number");
        int id = s.nextInt();
        int bikeIndex = id - 1;
        int getFirstBikeId = bikeList.get(0).getId();
        int getLastBikeId = bikeList.get(bikeList.size() - 1).getId();
        if ((id < getFirstBikeId || id > getLastBikeId) && id != 0) {
            System.out.println("There is no bike with provided ID, try again");
            reservedBike();
        } else if (id == 0) {
//            return;
        } else if (bikeList.get(bikeIndex).isOccupied() == true) {
            System.out.println("This bike is already occupied, choose another bike");
            reservedBike();
        } else {
            System.out.println("You have chosen following bike: "
                    + bikeList.get(bikeIndex).toString());
        }
        rentedBikeId = bikeIndex;
    }

    private void startRenting() {
        if (rentedBikeId == -1) {
            bikeReservationMenu();
        } else {
            startRenting = LocalTime.now();
            System.out.println("Started at " + formatter.format(startRenting));
            bikeList.get(rentedBikeId).setOccupied(true);
        }
    }


    private void showRentSummary() {
        System.out.println("Start time: " + formatter.format(startRenting));
        System.out.println("End time: " + formatter.format(stopRenting));
        System.out.println("Total cost: " + totalCost + " PLN");
        System.out.println("Wallet balance: " + person.getAmountOfMoney());

    }

    private boolean calculateRentCosts() {
        long differenceInMillis = ChronoUnit.MILLIS.between(startRenting, stopRenting);
        long seconds = differenceInMillis / 1000;
        long minutes = seconds / 60;
        totalCost = 0;
        totalCost = bikeList.get(rentedBikeId).getPricePerHour() * minutes + bikeList.get(rentedBikeId).getPricePerHour();
        if (totalCost > person.getAmountOfMoney()) {
            System.out.println("Add funds to your wallet first!");
            return false;
        } else {
            person.setAmountOfMoney(person.getAmountOfMoney() - totalCost);
            return true;
        }
    }

    private void addMoneyToTheWallet() {
        Scanner s = new Scanner(System.in);
        System.out.println("Type how much money you'd like to add: ");
        double amount = s.nextDouble();
        double checkWallet = person.getAmountOfMoney();
        person.setAmountOfMoney(checkWallet + amount);
        System.out.println("Wallet balance: " + person.getAmountOfMoney());
        runMenu();
    }

}
