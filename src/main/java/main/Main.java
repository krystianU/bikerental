package main;

import main.bikeDatabase.Bike;
import main.bikeDatabase.BikeDatabase;

public class Main {

    public static void main(String[] args) {
        RentController rentController = new RentController();
        rentController.runProgram();

    }
}
